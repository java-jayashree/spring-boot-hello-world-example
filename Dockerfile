FROM java:8
RUN mkdir -p /java_f
WORKDIR /java_f
COPY target/spring-boot-hello-world-example-0.0.1-SNAPSHOT.jar /java_f/spring-boot-hello-world-example-0.0.1-SNAPSHOT.jar
CMD java -jar spring-boot-hello-world-example-0.0.1-SNAPSHOT.jar
